<?php

class user_model extends CI_Model
{
public function insertRegisterData($data){
    //print_r($data);exit;
    $this->db->insert('user_info', $data);
    
    
}

public function showData(){
    //print_r($data);exit;
    $this->db->select('*');
    $this->db->from('user_info');
    $this->db->order_By('uid','desc');
    $query = $this->db->get()->result_array();
    return $query;
    //print_r($query);exit;
    
}

public function getEditData($uid){
    $this->db->where('uid',$uid);
    return $this->db->get('user_info')->row_array();
}
public function update_user($update_id,$update_array){
    $this->db->where('uid',$update_id);
    $this->db->update('user_info',$update_array);
}
public function delete_user($uid){
    $this->db->where('uid',$uid);
    $this->db->delete('user_info');
}

public function pdfData(){
    //print_r($data);exit;
    $this->db->select('*');
    $this->db->from('user_info');
    $this->db->order_By('uid','desc');
    $query = $this->db->get()->result_array();
    
    $html="<div class='table-responsive' style='margin-top: 50px;'>
    <table id='myTable' class='table table-bordered' style='width: 100%;'>
        <thead>
          <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Gender</th>
            <th>City</th>
            <th>edit</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>";
        foreach($query as $value){ 
            $html .="<tr>
    
            <td>".$value['fname']."</td>
            <td>".$value['lname']."</td>
            <td>".$value['gender']."</td>
            <td>".$value['city']."</td>
            <td><a href=".base_url()."Home/edit/".$value['uid']." class='btn btn-primary'>Edit</a></td>
                            <td><a href=".base_url()."Home/delete/".$value['uid']." class='btn btn-danger' >Delete</a></td>
          </tr>";
        }
        $html .="</tbody>
      </table>
      </div>";

    return $html;
}

}
?>    