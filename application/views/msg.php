<?php
if ($this->session->userdata('msg')) {
?>
    <div class="alert alert-<?php echo $this->session->userdata('type'); ?> col-md-12">
        <div class="card-content white-text">
          <p> <?php echo $this->session->userdata('msg'); ?> </p>
        </div>
       <button aria-label="Close" data-dismiss="alert" class="close white-text" type="button">
            <span aria-hidden="true">×</span>
        </button>
     </div>
<?php
}
$this->session->unset_userdata('msg');
$this->session->unset_userdata('type');
?>