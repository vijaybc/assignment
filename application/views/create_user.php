<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
</head>
<body>
	
	<div class="container">
		<div class="row">
			<div  class="col-md-12">
				<?php 
					$success = $this->session->userdata('success');
					$failure = $this->session->userdata('failure');
					if($success !=''){ ?>
						<div class="alert alert-success"><?php echo $success; ?></div>
					<?php }
					if($failure !=''){ ?>
						<div class="alert alert-failure"><?php $failure; ?></div>
					<?php }
				?>
			</div>
		</div>
		<h3>Create User</h3>
		<a class="pull-right btn btn-warning btn-large" style="margin-right:40px" href="<?php echo base_url()?>Home/save_pdf"><i class="fa fa-file-excel-o"></i> PDF Data</a>
		<hr>
		<form name="createUser" method="post" action="<?php echo base_url().'Home/create'; ?>" >
		<div class="row">
			
			
			<div class="col-md-6">
				<div class="form-group">
					<label>First Name</label>
					<input type="text" name="fname" id="fname" value="" class="form-control">
					 
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input type="text" name="lname" id="lname" value="" class="form-control">
					
				</div>
				<div class="form-group">
					<label>Gender</label>
					<input type="radio" name="gender" id="male"  value="male" class="">Male
                    <input type="radio" name="gender" id="female" value="female" class="">Female
				
				</div>
				<div class="form-group">
					<label>City</label>
					<input type="passwword" name="city" id="city" value="" class="form-control">
					
				</div>
				<div class="form-group">
					<button class="btn btn-primary">Create</button>
					<a href="<?php echo base_url().'Home/index'; ?>" class="btn btn-secondary">Cancel</a>
					
				</div>
				
			</div>
		
		</div>
	</form>
		
	</div>

</body>
</html>