<!DOCTYPE html>

<head>
  <title></title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <script src="<?php echo base_url(); ?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
  
       


<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js" type="text/javascript"></script> 
	
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script> 


<style>
.container {
   margin-top: 10px;
   margin-bottom: 10px;
}
</style>
</head>
<body>




<div class="container">

<div class="col-md-12" id="successMessage">
    <?php $this->load->view('msg'); ?>
</div>
<div class="row">


<div>
	<a href="<?php echo base_url().'Home/create'; ?>" style="float: right;" class="btn btn-primary">Create</a>					
</div>

<a class="pull-right btn btn-warning btn-large" style="margin-right:40px" href="<?php echo base_url()?>Home/save_pdf"><i class="fa fa-file-excel-o"></i> PDF Data</a>
<div class="table-responsive" style="margin-top: 50px;">
<table id="myTable" class="table table-bordered table-hovered" style="width: 100%;">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Gender</th>
        <th>City</th>
        <th>edit</th>
        <th>delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($registerData as $value){ ?>
      <tr>

        <td><?php echo $value['fname']; ?></td>
        <td><?php echo $value['lname']; ?></td>
        <td><?php echo $value['gender']; ?></td>
        <td><?php echo $value['city']; ?></td>
        <td><a href="<?php echo base_url().'Home/edit/'.$value['uid']; ?>" class="btn btn-primary">Edit</a></td>
						<td><a href="<?php echo base_url().'Home/delete/'.$value['uid']; ?>" class="btn btn-danger" onclick="return checkDel();">Delete</a></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
  </div>
</div>
</div>






</body>

<script>
function checkDel(){
  if(confirm("Are sure want delete ?")){
      return true;
  }else{
    return false;
  }
}

$(document).ready( function () {
    $('#myTable').DataTable();
});

</script>
</html>


