<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
</head>
<body>
	
	<div class="container">
		<h3>Update User</h3>
		<hr>
		<form name="createUser" method="post" action="<?php echo base_url().'Home/edit/'.$edit_user['uid'] ?>" >
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>First Name</label>
					<input type="text" name="fname" id="fname" value="<?php echo $edit_user['fname']; ?>" class="form-control">
					
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input type="text" name="lname" id="lname" value="<?php echo $edit_user['lname']; ?>" class="form-control">
					
				</div>
				<div class="form-group">
					<label>Gender</label>&nbsp;&nbsp;
                    <?php if( $edit_user['gender'] == 'male'){ ?>
                        <input type="radio" name="gender" value="male" checked="checked" class="">&nbsp; Male &nbsp;
                        <input type="radio" name="gender" value="female" class="">&nbsp; Female &nbsp;
                   <?php }else{ ?>
                        <input type="radio" name="gender" value="male" class="">&nbsp; Male &nbsp;
                    <input type="radio" name="gender" value="female" checked="checked" class="">&nbsp; Female &nbsp;
                  <?php  } ?>
					
					
				</div>
				<div class="form-group">
					<label>City</label>
					<input type="text" name="city" id="city" value="<?php echo $edit_user['city']; ?>" class="form-control">
					
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
					<a href="<?php echo base_url().'Home/index'; ?>" class="btn btn-secondary">Cancel</a>
					
				</div>
				
			</div>
		
		</div>
	</form>
		
	</div>

</body>
</html>