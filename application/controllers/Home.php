<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        //$this->load->helper('pdf_helper');
        $this->load->library('Pdf');
        
    }
	
	public function index()
	{
        echo "working";
        echo "working 2";
        echo "working 3";
        //echo $this->db->version();
        $data['page'] = 'export-pdf';
        $data['title'] = 'Export PDF data | Web Preparations';
        
        

        $data['registerData'] = $this->user_model->showData();
        //echo '<pre>';
        //print_r($data['registerData']);
		$this->load->view('user_info',$data);
    }

    // public function save_pdf(){ 
    //     //load mPDF library
    //     $this->load->library('m_pdf'); 
    //     //now pass the data//
    //     $data['mobiledata'] = $this->pdf->mobileList();
    //     $html=$this->load->view('user_info',$data, true); //load the pdf.php by passing our data and get all data in $html varriable.
    //     $pdfFilePath ="webpreparations-".time().".pdf"; 
    //     //actually, you can pass mPDF parameter on this load() function
    //     $pdf = $this->m_pdf->load();
    //     //generate the PDF!
    //     $stylesheet = '<style>'.file_get_contents('assets/css/bootstrap.min.css').'</style>';
    //     // apply external css
    //     $pdf->WriteHTML($stylesheet,1);
    //     $pdf->WriteHTML($html,2);
    //     //offer it to user via browser download! (The PDF won't be saved on your server HDD)
    //     $pdf->Output($pdfFilePath, "D");
    //     exit;
    // }

    public function create(){
		/*$this->load->model('user_modal');*/
		//$this->load->view('create');

		if(empty($_POST)) {
            $this->load->view('create_user');
        }else{
            echo '<pre>';
            print_r($_POST);
            $formArr = array();
			$formArr['fname'] = $this->input->post('fname');
			$formArr['lname'] = $this->input->post('lname');
			$formArr['gender'] = $this->input->post('gender');
			$formArr['city'] = $this->input->post('city');
			//print_r($formArr);
			//$formArr['addeddate'] = date('Y-m-d');
			$this->user_model->insertRegisterData($formArr);
			$this->session->set_flashdata('success','Record Successfully inserted');
			redirect(base_url().'Home/index');
        }
			
		
			
		
    }
    
    public function insert_user(){
        $data = $this->input->post();
        //print_r($data);exit;
        $this->user_model->insertRegisterData($data);
        //exit;
    }

    function edit($id){
        $data['edit_user'] = $this->user_model->getEditData($id);
        //echo '<pre>';
        //print_r($data);
    
        if(empty($_POST)) {
            $this->load->view('edit',$data);
        }else{
            
            $update_data = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'gender' => $this->input->post('gender'),
                'city' => $this->input->post('city')
             );
                
            // print_r($update_data);
            // exit;
    
            $this->user_model->update_user($id,$update_data);
            //echo $this->db->last_query();
            $this->session->set_flashdata('success','Record Updated Successfully');
            redirect(base_url().'Home/index');
        }
        
    }
    public function delete($uid){
		$this->user_model->delete_user($uid);
		$this->session->set_flashdata('success','User Deleted Successfully');
		redirect(base_url().'Home/index');
    }
    
    public function save_pdf(){
       $html =  $this->user_model->pdfData();
       $this->pdf->loadHtml($html);
       $this->pdf->render();
       $this->pdf->stream("pdfVij.pdf", array("Attachment"=>0));
    }
}



// Laravel
// Should know the MVC concept.
// Should have experience in working with Agile SDLC.
// Experience in rest API and common third-party APIs (Google, Facebook, eBay, etc).
// Experience in PHP Kit integration.
// Should know OOPS
// Experience in designing Database Architecture(Mysql)